#!/usr/bin/env python3
#
# Impulsar un toot tomando su id de un fichero de texto

# CONSTANTES
INST_URL = 'https://botsin.space/'
NOMFICH = 'toots.txt'
MAX_TOOT = 500

# LIBRERIAS
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from lxml import html
from mastodon import Mastodon
from random import randint
import requests

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica hilos al azar' )
    parser.add_argument( '-t', '--app_token', help='Token de la app', type=str )
    return parser.parse_args()

def Url_toot():
    lista_toots = []
    fich = open ( NOMFICH, 'r' )
    lista_toots = fich.readlines()
    fich.close()

    num_toots = len( lista_toots )
    n_toot = randint( 0, num_toots - 1 )
    url_toot = lista_toots[ n_toot ].strip()

    return url_toot

def Id_toot( url_toot ):
    id_toot = url_toot[ url_toot.find( '@' ) + 1 : len( url_toot ) ]
    id_toot = id_toot[ id_toot.find( '/' ) + 1 : len( id_toot ) ]
    return id_toot

def cuerpo( url, cabecera ):
    bsObj = BeautifulSoup( requests.get( url ).content, 'lxml' )
    cadena = str( bsObj.find( 'div', { 'class': 'e-content' } ) )
    doc = html.document_fromstring( cadena )
    cadena = doc.text_content()
    cadena = cadena.strip()
    
    final = '\n\n' + url
    max_long = MAX_TOOT - len( cabecera ) - len( final )
    if len( cadena ) > max_long:
        cad_union = ' [...]'
        max_long = max_long - len( cad_union )
        cadena = cadena[ 0 : max_long ] + ' [...]'
    cadena = cabecera + cadena + final

    return cadena

def reimpulsar( url, api ):
    # https://mastodon.social/@LaCuriosidadDelGato/104025127677579898
    id = url.split( '/' )
    try:
        api.status( id[-1]) 
        id = id[-1]
    except:
        return 'No existe el estado'
    
    try:
        status = api.status_unreblog( id )
        time.sleep(5)
    except:
        pass
    
    try:
        api.status_reblog( id )
        return 'Impulso hecho'
    except:
        return 'No se puede impulsar'

# MAIN
args = _args()
masto = Mastodon( access_token = args.app_token, api_base_url = INST_URL )
url_toot = Url_toot()
id_toot = Id_toot( url_toot )
mensaje = cuerpo( url_toot, 'Mensaje de prueba para @francistein@mastodon.social\n\n' )
print( 'URL = {', url_toot, '}' )
print( 'ID TOOT = {', id_toot, '}' )
print( 'MENSAJE = {', mensaje, '}' )
# masto.status_post( mensaje )
reimpulsar( url_toot, INST_URL )
