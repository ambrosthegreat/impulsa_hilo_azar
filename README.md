# Impulsar un hilo al azar desde un fichero con hilos guardados

___19/04/2020___
Impulsar un hilo al azar de @francistein@mastodon.social desde un fichero con hilos guardados.

___31/05/2020___
Intentando darle la funcionalidad para que el autor de los hilos pueda modificar el bot sin necesidad de tener acceso al servidor, vía comandos desde mensajes directos en Mastodon.
El script es fran3.py y depende de mi_masto.py, mi_ficheros.py y mi_dropbox.py.
Se lanza con run_escuchar.sh para procesar los comandos o con run_reenviar.sh para reenviar los hilos. La idea es tener ambas tareas en el heroku-scheduler.
