#!/usr/bin/env python3
#
# mi_masto
# Librería para interactuar con mastodon
#

# CONSTANTES
MAX_NOTIF = 200 # Máximo número de notificaciones a leer

# LIBRERIAS
from lxml import html
from mastodon import Mastodon

# FUNCIONES
class mi_masto():
    def __init__( self, instancia, masto_token ):
        self.masto = Mastodon( access_token = masto_token, api_base_url = instancia )
        print( 'mi_masto() => Conectado', self.masto.account_verify_credentials()['username'] )

    def reimpulsa( self, url_hilo ):
        # Reimpulsa (si existe) un hilo dada la url de su primer toot
        estado = self.masto.search_v2( url_hilo )

        if len( estado.statuses ) != 1:
            print( 'reimpulsa() => No existe el estado ' + str( url_hilo ) )
            return 'No existe el estado'
    
        id_toot = estado.statuses[0].id
    
        try:
            status = self.masto.status_unreblog( id_toot )
            time.sleep( 2 )
            print( 'reimpulsa() => Desmarcado ' + str( url_hilo ) )
            return 'Impulsado ' + str( url )
        except:
            pass
    
        try:
            self.masto.status_reblog( id_toot )
            print( 'reimpulsa() => Impulsado ' + str( url_hilo ) )
            return 'Impulso hecho'
        except:
            print( 'reimpulsa() => No se puede impulsar ' + str( url_hilo ) )
            return 'No se puede impulsar'

    def leer_notificaciones( self, num_mensa = MAX_NOTIF, ultima = None ):
        # Leer las notificaciones de la cuenta desde ultima
        if ultima == None:
            notifs = self.masto.notifications( limit = num_mensa, mentions_only = True )
        else:
            notifs = self.masto.notifications( limit = num_mensa, mentions_only = True, since_id = ultima )
        return notifs

    def cuenta( self, notificacion ):
        # Devuelve la cuenta desde la que ha llegado una notificación
        return notificacion['account']['acct']

    def es_mencion( self, notificacion ):
        # True si la notificación es una mención
        return notificacion.type == 'mention'

    def cuerpo( self, notificacion ):
        # Devuelve el texto de la notificación
        doc = html.document_fromstring( notificacion['status']['content'] )
        # Preserve end-of-lines
        # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
        for br in doc.xpath("*//br"):
            br.tail = "\n" + br.tail if br.tail else "\n"
        for p in doc.xpath("*//p"):
            p.tail = "\n" + p.tail if p.tail else "\n"
        return doc.text_content()

    def id_notif( self, notificacion ):
        # Devuelve la id de la notificacion
        return notificacion['id']

    def id_mencion( self, notificacion ):
        # Devuelve la id del mensaje que ha hecho la mención
        return notificacion['status']['id']

    def mensaje( self, texto, respuesta_a = None, visibilidad = 'direct' ):
        if respuesta_a == None:
            mi_toot = self.masto.status_post( texto, visibility = visibilidad )
        else:
            mi_toot = self.masto.status_post( texto, in_reply_to_id = respuesta_a, visibility = visibilidad )
        return mi_toot
