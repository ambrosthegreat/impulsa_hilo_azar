#!/usr/bin/env python3
#
# fran3
# Impulsar un toot tomando su id de un fichero de texto
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
# Trabajar con ficheros en dropbox: https://villoro.com/post/dropbox_python

# CONSTANTES
INST_URL = 'https://botsin.space/'
MARCA = '[X]'
FICH_NOTIF = 'notificaciones.nfo'
NUM_MENSA = 20
LISTA_AUTORIZADOS = [ 'AmbrosTheGreat@hispatodon.club', 'francistein@mastodon.social' ]
TEMPORAL = 'temporal.tmp'

# LIBRERIAS
from argparse import ArgumentParser
from lxml import html
from mastodon import Mastodon
from os import getcwd
from os.path import isfile as existe_fichero
from random import randint
import mi_dropbox
import time

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica hilos al azar' )
    parser.add_argument( '-d', '--dropbox_token', help='Token de dropbox', type=str )
    parser.add_argument( '-f', '--fich_toots', help='Fichero con los enlaces a los toots a impulsar', type=str )
    parser.add_argument( '-t', '--app_token', help='Token de la app', type=str )
    return parser.parse_args()

def descarga_fich( dbx, nom_fich ):
    nom_dbx = '/' + nom_fich.split( '/' )[-1]
    try:
        dbox.files_download_to_file( nom_fich, nom_dbx )
        return True
    except:
        return False

def carga_lista_toots( nom_fich ):
    f = open( nom_fich )
    lineas = f.readlines()
    f.close()

    salida = []
    for linea in lineas:
        if linea.find( MARCA )<0:
            if linea.strip() != '':
                salida.append( linea.strip() )

    return salida

def resetea_marca( nom_fich ):
    f = open( nom_fich, 'r' )
    lineas = f.readlines()
    f.close()
    
    f = open( nom_fich, 'w' )
    for linea in lineas:
        linea = linea[ linea.find( MARCA ) + len( MARCA ) : len( linea ) ].strip()
        if len( linea ) > 0:
            f.write( linea + '\n' )
    f.close()

def descarga_toots( dbx, nom_fich ):
    if descarga_fich( dbx, nom_fich ):
        salida = carga_lista_toots( nom_fich )
        if len( salida ) == 0:
            resetea_marca( nom_fich )
            salida = carga_lista_toots( nom_fich )
    else:
        salida = []
    return salida

def elige_al_azar( mi_lista ):
    if len( mi_lista ) > 0:
        numAleatorio = randint( 0, len( mi_lista ) - 1 )
        salida = mi_lista[numAleatorio]
    else:
        salida = ''
    return salida

def salva_marca( nom_fich, cadena ):
    f = open( nom_fich, 'r' )
    lineas = f.readlines()
    f.close()

    lista = []
    for linea in lineas:
        if linea.strip() != '':
            if linea.strip() != cadena:
                lista.append( linea.strip() )
            else:
                lista.append( MARCA + ' ' + linea.strip() )

    f = open( nom_fich, 'w' )
    for linea in lista:
        f.write( linea + '\n' )
    f.close()

def carga_toots( dbx, nom_fich ):
    nom_dbx = nom_fich.split( '/' )[-1]
    nom_dbx = '/' + nom_dbx
    dbox.files_upload( open( nom_fich, 'rb' ).read(), nom_dbx, WriteMode( 'overwrite', None ) )

def reimpulsar( api, dbox, nom_fich_toots ):
    nom_fich = getcwd() + '/' + nom_fich_toots
    lista_toots = descarga_toots( dbox, nom_fich )
    url = elige_al_azar( lista_toots )

    estado = api.search_v2( url )
    if len( estado.statuses ) != 1:
        return 'No existe el estado'

    id_toot = estado.statuses[0].id

    """
    try:
        status = api.status_unreblog( id_toot )
        time.sleep( 2 )
        return 'Impulsado ' + str( url )
    except:
        pass
    
    try:
        api.status_reblog( id_toot )
        return 'Impulso hecho'
    except:
        return 'No se puede impulsar'
    """
    
    salva_marca( nom_fich, url )
    carga_toots( dbox, nom_fich )
    return 'Impulsado ' + str( url )

def cuerpo_notif( notificacion ):
    # La API de Mastodon API devuelve el contenido del toot en HTML
    doc = html.document_fromstring( notificacion['status']['content'] )
    # Preserve end-of-lines
    # <https://stackoverflow.com/questions/18660382/how-can-i-preserve-br-as-newlines-with-lxml-html-text-content-or-equivalent>
    for br in doc.xpath("*//br"):
        br.tail = "\n" + br.tail if br.tail else "\n"
    for p in doc.xpath("*//p"):
        p.tail = "\n" + p.tail if p.tail else "\n"
    return doc.text_content()

def leer_ultima_notif( nom_fich ):
    if existe_fichero( nom_fich ):
        fichero = open( nom_fich, 'r' )
        salida = fichero.read()
        fichero.close()
        if salida != None:
            if salida != 'None':
                salida = int( salida )
            else:
                salida = None
        else:
            salida = None
    else:
        salida = None
    return salida

def guardar_ultima_notif( nom_fich, id_notif ):
    fichero = open( nom_fich, 'w' )
    fichero.write( str( id_notif ) )
    fichero.close()

    from os.path import isfile as existe_fichero# Subir el archivo a dropbox
    nom_dbx = nom_fich.split( '/' )[-1]
    nom_dbx = '/' + nom_dbx
    dbox.files_upload( open( nom_fich, 'rb' ).read(), nom_dbx, WriteMode( 'overwrite', None ) )

def texto_ayuda():
    ayuda = 'Lista de comandos del bot:'
    ayuda = ayuda + '\n---\n'
    ayuda = ayuda + '\n\t<a>\tAñadir hilo'
    ayuda = ayuda + '\n\t<b>\tBorrar hilo'
    ayuda = ayuda + '\n\t<h>\tEsta ayuda'
    ayuda = ayuda + '\n\t<l>\tListado de hilos'
    return ayuda

def lista_hilos( dbx, fich_hilos ):
    try:
        salida = str( dbx.files_get_temporary_link( '/' + nom_hilos ) )
        salida = salida[ salida.find( 'link=\'' ) + len( 'link=\'' ) : len( salida ) ]
        salida = salida[ 0 : salida.find( '\'' ) ]
        salida = salida.strip()
        return salida
    except:
        return 'No existe el fichero de hilos'

def anadir_hilo( dbx, nom_hilos, texto ):
    nom_fich = getcwd() + '/' + nom_hilos
    nom_dbx = '/' + nom_fich.split( '/' )[-1]
    nom_tmp = getcwd() + '/' + TEMPORAL

    hilo_nuevo = texto[ texto.find( '<a>' ) : len( texto ) ].strip()
    hilo_nuevo = hilo_nuevo.split( ' ' )[0]

    hilo_existe = False
    hilos = []
    try:
        dbx.files_download_to_file( nom_tmp, nom_dbx )
        f = open( nom_tmp )
        lineas = f.readlines()
        f.close()
        for linea in lineas:
            if linea.strip() != '':
                hilos.append( linea )
                if linea.find( hilo_nuevo ):
                    hilo_existe = True
    except:
        print( 'anadir_hilo() => No existe fichero de hilos en dropbox' )

    try:
        if not( hilo_existe ):
            hilos.append( texto )
            f = open( nom_tmp, 'w' )
            for linea in hilos:
                f.write( linea + '\n' )
            f.close()
            dbx.files_upload( open( nom_tmp, 'rb' ).read(), nom_dbx, WriteMode( 'overwrite', None ) )
            salida = 'Hilo ' + texto + ' añadido'
            print( 'anadir_hilo() => Hilo añadido' )
        else:
            salida = 'Hilo ' + texto + ' ya existía'
            print( 'anadir_hilo() => Hilo no añadido' )
    except:
        print( 'anadir_hilo() => No es posible añadir el hilo ' + texto )
        salida = 'Error ' + str( sys.exc_info()[0] )

    return salida

def borrar_hilo( dbx, nom_hilos, hilo_borrar ):
    return 'Borrar hilo'

def leer_comandos( api, dbx, nom_hilos ):
    nom_fich = getcwd() + '/' + FICH_NOTIF
    if descarga_fich( dbx, nom_fich ):
        ult_notif = leer_ultima_notif( nom_fich )
    else:
        ult_notif = None
    print( 'leer_comandos() => Última notificación ' + str( ult_notif ) )
    notifs = api.notifications( limit = NUM_MENSA, mentions_only = True, since_id = ult_notif )
    print( 'leer_comandos() => Leídas ' + str( len( notifs ) ) + ' notificaciones' )
    
    ultima_id = None
    for notif in notifs:
        cuenta = notif['account']['acct']
        if notif.type == 'mention':
            texto = cuerpo_notif( notif )
            id_notif = notif['status']['id']
            ultima_id = id_notif
        else:
            texto = ''
            id_notif = None
        
        if cuenta in LISTA_AUTORIZADOS:
            txt_enviar = ''
            texto = texto.lower()
            print( 'leer_comandos() => Procesando: ' + texto )
            if texto.find( '<a>' )>0:
                txt_enviar = anadir_hilo( dbx, nom_hilos, texto )
            if texto.find( '<b>' )>0:
                txt_enviar = borrar_hilo( dbx, nom_hilos, texto )
            if texto.find( '<h>' )>0:
                txt_enviar = texto_ayuda()
            if texto.find( '<l>' )>0:
                txt_enviar = lista_hilos( dbx, nom_hilos )

            if txt_enviar != '':
                txt_enviar = '@' + str( cuenta ) + '\n' + txt_enviar
                print( 'leer_comandos() => Respuesta = ' + txt_enviar )
                # api.status_post( txt_enviar, in_reply_to_id = id_notif, visibility = 'direct' )

    if ultima_id != None:
        guardar_ultima_notif( nom_fich, ultima_id )

# MAIN
args = _args()
masto = Mastodon( access_token = args.app_token, api_base_url = INST_URL )
dbx = mi_dropbox( args.dropbox_token )
leer_comandos( masto, dbox, args.fich_toots )
# mensaje = reimpulsar( masto, dbox, args.fich_toots )
# print( mensaje )
