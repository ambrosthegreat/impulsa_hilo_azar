#!/usr/bin/env python3
#
# fran3
# Impulsar un toot tomando su id de un fichero de texto
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
# Trabajar con ficheros en dropbox: https://villoro.com/post/dropbox_python

# CONSTANTES
FICH_NOTIF = 'notificaciones.nfo'
LISTA_AUTORIZADOS = [ 'AmbrosTheGreat@hispatodon.club', 'francistein@mastodon.social' ]
MARCA = '[X]'
NUM_MENSA = 20
TEMPORAL = 'temporal.tmp'

# LIBRERIAS
from argparse import ArgumentParser
from mi_dropbox import mi_dropbox
from mi_ficheros import mi_ficheros
from mi_masto import mi_masto
from random import randint
import sys

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica hilos al azar' )
    parser.add_argument( '-d', '--dropbox_token', help='Token de dropbox', type=str )
    parser.add_argument( '-f', '--fich_toots', help='Fichero con los enlaces a los toots a impulsar', type=str )
    parser.add_argument( '-i', '--inst_url', help='URL de la instancia', type=str )    
    parser.add_argument( '-n', '--notificaciones', help='Tratar notificaciones', action="store_true", default=False )
    parser.add_argument( '-r', '--reimpulsar', help='Reimpulsar un hilo al azar', action="store_true", default=False )
    parser.add_argument( '-t', '--app_token', help='Token de la app', type=str )
    return parser.parse_args()

def quita_con_marca( lista ):
    # Elimina de la lista los elementos que contengan la MARCA
    salida = []
    for elemento in lista:
        if elemento.find( MARCA )<0:
            if elemento.strip() != '':
                salida.append( elemento.strip() )
    return salida

def elige_al_azar( mi_lista ):
    if len( mi_lista ) > 0:
        numAleatorio = randint( 0, len( mi_lista ) - 1 )
        salida = mi_lista[numAleatorio]
    else:
        salida = ''
    return salida

def salva_marca( f_toots, cadena ):
    lineas = f_toots.lee_lineas()

    lista = []
    for linea in lineas:
        if linea.strip() != '':
            if linea.strip() != cadena:
                lista.append( linea.strip() )
            else:
                lista.append( MARCA + ' ' + linea.strip() )

    f_toots.escribe_lista( lista )

def reimpulsar( masto, dbx, nom_fich_toots ):
    # Reimpulsar un hilo al azar desde el fichero nom_fich_toots en dropbox
    f_toots = mi_ficheros( nom_fich_toots )
    if dbx.descargar( f_toots.en_path(), f_toots.en_raiz() ):
        lista_toots = f_toots.lee_lineas()
        lista_toots = quita_con_marca( lista_toots )

        url = elige_al_azar( lista_toots )
        print( 'reimpulsar() => Impulsado ' + str( url ) )
        salida = masto.reimpulsa( url )

        salva_marca( f_toots, url )
        dbx.cargar( f_toots.en_path(), f_toots.en_raiz() )
    else:
        print( 'reimpulsar() => No se ha podido descargar el fichero de hilos' )
        salida = 'ERROR descargando fichero de hilos'
    return salida

def existe_en_lista( elemento, lista ):
    # Verdadero si el elemento está en la lista
    salida = False
    for cada_uno in lista:
        if cada_uno.find( elemento ) >= 0:
            salida = True
    return salida

def anadir_hilo( dbx, nom_hilos, texto ):
    # Añade un nuevo hilo al fichero de hilos sin tocar las marcas del resto
    hilo_nuevo = texto[ texto.find( '<a>' ) + len( '<a>' ) : len( texto ) ].strip()
    hilo_nuevo = hilo_nuevo.split( ' ' )[0]

    f_hilos = mi_ficheros( nom_hilos )
    f_temporal = mi_ficheros( TEMPORAL )
    if dbx.descargar( f_temporal.en_path(), f_hilos.en_raiz() ):
        lineas = f_temporal.lee_lineas()
    else:
        lineas = []
        print( 'anadir_hilo() => No existe fichero de hilos en dropbox' )

    if not( existe_en_lista( hilo_nuevo, lineas ) ):
        lineas.append( hilo_nuevo )
        f_temporal.escribe_lista( lineas )
        dbx.cargar( f_temporal.en_path(), f_hilos.en_raiz() )
        salida = 'Hilo ' + hilo_nuevo + ' añadido'
        print( 'anadir_hilo() => Hilo añadido' )
    else:
        salida = 'Hilo ' + hilo_nuevo + ' ya existía'
        print( 'anadir_hilo() => Hilo no añadido' )

    f_temporal.borrar()
    return salida

def borrar_de_lista( elemento, lista ):
    # Borra un elemento de una lista
    salida = []
    for cada_uno in lista:
        cada_uno = cada_uno.strip()
        if cada_uno.find( elemento ) < 0:
            if cada_uno != '':
                salida.append( cada_uno )
    return salida

def borrar_hilo( dbx, nom_hilos, texto ):
    # Borra un hilo de fichero de hilos
    f_hilos = mi_ficheros( nom_hilos )
    f_temporal = mi_ficheros( TEMPORAL )
    elemento = texto[ texto.find( '<b>' ) + len( '<b>' ) : len( texto ) ].strip().split( ' ' )[0]
    if dbx.descargar( f_temporal.en_path(), f_hilos.en_raiz() ):
        lineas = f_temporal.lee_lineas()
        lineas = borrar_de_lista( elemento, lineas )
        f_temporal.escribe_lista( lineas )
        dbx.cargar( f_temporal.en_path(), f_hilos.en_raiz() )
        salida = 'Hilo ' + elemento + ' borrado'
        print( 'borrar_hilo() => Hilo borrado ' )
    else:
        lineas = []
        salida = 'No existe fichero de hilos'
        print( 'borrar_hilo() => No existe fichero de hilos en dropbox' )
    f_temporal.borrar()
    return salida

def texto_ayuda():
    ayuda = 'Lista de comandos del bot:'
    ayuda = ayuda + '\n---\n'
    ayuda = ayuda + '\n\t<a>\tAñadir hilo'
    ayuda = ayuda + '\n\t<b>\tBorrar hilo'
    ayuda = ayuda + '\n\t<h>\tEsta ayuda'
    ayuda = ayuda + '\n\t<l>\tListado de hilos'
    return ayuda

def lista_hilos( dbx, fich_hilos ):
    # Devuelve un enlace al fichero de hilos
    return dbx.enlace( fich_hilos )

def leer_comandos( masto, dbx, nom_hilos ):
    # Procesar comandos que se pasan vía toot para los usuarios autorizados
    f_notif = mi_ficheros( FICH_NOTIF )
    if dbx.descargar( f_notif.en_path(), f_notif.en_raiz() ):
        ult_notif = f_notif.primera_linea()
    else:
        ult_notif = None
    print( 'leer_comandos() => Última notificación ' + str( ult_notif ) )
    notifs = masto.leer_notificaciones( NUM_MENSA, ult_notif )
    print( 'leer_comandos() => Leídas ' + str( len( notifs ) ) + ' notificaciones' )
    
    ultima_id = None
    for notif in reversed( notifs ):
        if masto.es_mencion( notif ):
            texto = masto.cuerpo( notif )
            ultima_id = masto.id_notif( notif )
        else:
            texto = ''
            id_notif = None

        if masto.cuenta( notif ) in LISTA_AUTORIZADOS:
            txt_enviar = ''
            texto = texto.lower()
            print( 'leer_comandos() => Procesando: ' + texto )
            if texto.find( '<a>' )>0:
                txt_enviar = anadir_hilo( dbx, nom_hilos, texto )
            if texto.find( '<b>' )>0:
                txt_enviar = borrar_hilo( dbx, nom_hilos, texto )
            if texto.find( '<h>' )>0:
                txt_enviar = texto_ayuda()
            if texto.find( '<l>' )>0:
                txt_enviar = lista_hilos( dbx, nom_hilos )

            if txt_enviar != '':
                cuenta = masto.cuenta( notif )
                try:
                    txt_enviar = '@' + str( cuenta ) + '\n' + str( txt_enviar )
                    print( 'leer_comandos() => Respuesta = ' + txt_enviar )
                    masto.mensaje( txt_enviar, masto.id_mencion( notif ), 'direct' )
                except:
                    print( 'leer_comandos() => ERROR ' + str( sys.exc_info()[0] ) )

    if ultima_id != None:
        f_notif.escribe_una_linea( ultima_id )
        dbx.cargar( f_notif.en_path(), f_notif.en_raiz() )

# MAIN
args = _args()
masto = mi_masto( args.inst_url, args.app_token )
dbx = mi_dropbox( args.dropbox_token )

if args.reimpulsar:
    mensaje = reimpulsar( masto, dbx, args.fich_toots )
elif args.notificaciones:
    leer_comandos( masto, dbx, args.fich_toots )
else:
    print( 'main() => No se ha especificado ninguna acción' )
