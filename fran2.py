#!/usr/bin/env python3
#
# Impulsar un toot tomando su id de un fichero de texto
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
INST_URL = 'https://botsin.space/'

# LIBRERIAS
from argparse import ArgumentParser
import time
from random import randint

from mastodon import Mastodon


# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica hilos al azar' )
    parser.add_argument( '-f', '--fich_toots', help='Fichero con los enlaces a los toots a impulsar', type=str )
    parser.add_argument( '-t', '--app_token', help='Token de la app', type=str )
    return parser.parse_args()

def url_toot( nomfich ):
    f = open( nomfich )
    urls = f.readlines()
    f.close()
    numAleatorio = randint( 0, len( urls ) - 1 )
    return urls[numAleatorio].strip()

def reimpulsar( url, api ):
    # https://botsin.space/@franhilos/104185339952559549
    print( url )
    estado = api.search_v2( url )
    
    if len( estado.statuses ) != 1:
        return 'No existe el estado'

    id_toot = estado.statuses[0].id


    try:
        status = api.status_unreblog( id_toot )
        time.sleep( 2 )
    except:
        pass
    
    try:
        api.status_reblog( id_toot )
        return 'Impulso hecho'
    except:
        return 'No se puede impulsar'
        
# MAIN
args = _args()
masto = Mastodon( access_token = args.app_token, api_base_url = INST_URL )
mensaje = reimpulsar( url_toot( args.fich_toots ), masto )       
print( mensaje )
